﻿using System.Collections.Generic;

namespace WebApplication4.Models
{
    public class Book
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ISBN { get; set; }
        public IEnumerable<Author> Authors { get; set; }
        public Publisher Publisher { get; set; }
    }
}
