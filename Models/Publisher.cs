﻿namespace WebApplication4.Models
{
    public class Publisher
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
