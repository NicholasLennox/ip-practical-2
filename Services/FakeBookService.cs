﻿using System.Collections.Generic;
using WebApplication4.Models;

namespace WebApplication4.Services
{
    public class FakeBookService : IBookService
    {
        public Book AddBook(Book book)
        {
            return book;
        }

        public void DeleteBook(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Book> GetAllBooks()
        {
            return new List<Book>();
        }

        public Book GetBook(int id)
        {
            return new Book();
        }

        public Book UpdateBook(Book book)
        {
            return book;
        }
    }
}
