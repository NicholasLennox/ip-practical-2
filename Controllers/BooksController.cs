﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using WebApplication4.Models;
using WebApplication4.Services;

namespace WebApplication4.Controllers
{
    [Route("api/v1/books")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private readonly IBookService _bookService;

        public BooksController(IBookService books)
        {
            _bookService = books;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Book>> GetAll()
        {
            return Ok(_bookService.GetAllBooks());
        }

        [HttpGet("{id}")]
        public ActionResult<Book> GetSpecific(int id)
        {
            if (id == 2)
                return NotFound();
            return Ok(_bookService.GetBook(id));
        }

        [HttpPost]
        public ActionResult AddBook(Book book)
        {
            Book ret = _bookService.AddBook(book);
            return CreatedAtAction(nameof(GetSpecific), new { id = ret.Id }, ret);
        }

        [HttpPut("{id}")]
        public ActionResult Update(int id, Book book)
        {
            if (id != book.Id)
                return BadRequest();
            _bookService.UpdateBook(book);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _bookService.DeleteBook(id);
            return NoContent();
        }
    }
}
